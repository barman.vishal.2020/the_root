# The_Root

Submission for Build a Better Tomorrow Hackathon on Hackerearth

## Instructions 

1. Install the libraries listed in requirements.txt however it is seen fit.

2.Manually Download the data from Kaggel: https://www.kaggle.com/c/planet-understanding-the-amazon-from-space/data?select=test-jpg-additional and then run Data_Collection.ipynb after adjusting the path names to the convenience.

3.Then you can proceed to train the models by running the Training.ipynb or Alternate_Training (Semi-Supervised) notebook. You will need to run this notebook five times, once for each of the following fastai models: resnet50, resnet101, resnet152, densenet121, densenet169. You can change which architecture you are using by changing the line in cell [8] to whichever pretrained model you wish to train, for example:
arch = models.resnet152

4.You can run Data_Exploratory_Analysis.ipynb and EDA.ipynb for date exploration with Dehazing.ipynb to preprocess the images with Tensor_Conversion.ipynb for the CNN and VGG-16 models. where tensor input are required.

5.For hyperparameter optimisation, the Thresholds.ipynb can be run to improve the score and Customize_CNN.py to customize the CNN to make it fine tuned and Custom_Metrics.py and eventually transfer the weights to the VGG-16 model through running Transfer_CNN-VGC16.py.

6.To make predictions , run the notebook Prediction.ipynb for each particular models.

7.To create an ensemble of the models you wish to include, you can run the Ensembling.ipynb notebook. Simply define the model_list in cell [4] to include the models you wish like so:
model_list = ['resnet50.pkl','resnet101.pkl', 'resnet152.pkl', 'densenet121.pkl', 'densenet169.pkl']

8. To make predictions , run the notebook Prediction.ipynb for the emsembled models. The Confusion_Matrix.ipynb needs to be run to generate the confusion matrix for the models with New_Imagery.py to test it on real life applications or images.


